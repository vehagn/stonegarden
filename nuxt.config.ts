import vuetify from 'vite-plugin-vuetify'
import {version} from './package.json'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    app: {
        head: {
            title: 'Stonegarden.dev',
            charset: 'utf-8',
            viewport: 'with=device-with, initial-scale=1',
            meta: [
                {name: 'description', content: 'Homepage with showcase links'},
                {name: 'robots', content: 'index, follow'}
            ],
        }
    },
    runtimeConfig: {
        public: {
            clientVersion: version,
        }
    },
    build: {
        transpile: ['vuetify'],
    },
    css: ['vuetify/styles'],
    vite: {
        ssr: {
            noExternal: ['vuetify']
        }
    },
    modules: [
        async (options, nuxt) => {
            nuxt.hooks.hook('vite:extendConfig', (config) =>
                // @ts-ignore
                config.plugins.push(vuetify())
            )
        }
    ]
});
